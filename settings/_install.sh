#!/bin/bash

# Switch to noninteractive mode
export DEBIAN_FRONTEND=noninteractive

# Skip virtualbox-ext-pack licence window
echo virtualbox-ext-pack virtualbox-ext-pack/license select true | sudo debconf-set-selections

# Remove packages
sudo apt -y remove geary

# Update & upgrade
sudo apt update
sudo apt -y upgrade


# Google Chrome and Google Earth
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
#sudo add-apt-repository "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main"
[[ -f /etc/apt/sources.list.d/google-chrome.list ]] || echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" | sudo tee /etc/apt/sources.list.d/google-chrome.list
#sudo add-apt-repository "deb [arch=amd64] http://dl.google.com/linux/earth/deb/ stable main"
[[ -f /etc/apt/sources.list.d/google-earth-pro.list ]] || echo "deb [arch=amd64] http://dl.google.com/linux/earth/deb/ stable main" | sudo tee /etc/apt/sources.list.d/google-earth-pro.list

# Update after setting apt sources
sudo apt update

# Software instalation
sudo apt install -y \
    vim  `# text editors` \
    texlive-full gummi  `# latex` \
    chromium thunderbird thunderbird-locale-cs google-chrome-stable  `# browser/e-mail` \
    python3 python3-pip perl nodejs npm php valgrind default-jre default-jdk scilab  `# programming` \
    steam  `# windows compatibility and games` \
    flameshot inkscape inkscape-open-symbols gimp  `# image tools` \
    blender kdenlive vlc obs-studio simplescreenrecorder youtube-dl  `# video tools` \
    audacity soundconverter easytag  `# audio tools` \
    graphviz gnuplot  `# graph visualization` \
    ntfs-3g mtp-tools  `# file system support` \
    remmina remmina-plugin-rdp remmina-plugin-vnc remmina-plugin-xdmcp remmina-plugin-spice freerdp2-x11  `# remote control` \
    mc doublecmd-qt krusader kdiff3 krename kate timeshift  `# file manager + plugins, backups` \
    git simple-scan screen picocom nmap lsof arp-scan lsscsi gparted gddrescue cifs-utils nfs-common tmux neofetch snapd virt-viewer network-manager-l2tp network-manager-l2tp-gnome  `# system utilities` \
    gnome-shell-extension-caffeine gnome-shell-extension-weather gnome-shell-extension-dash-to-panel gnome-shell-extension-system-monitor  `# gnome shell extensions` \
    qgis google-earth-pro-stable  `# maps and gis` \
    virtualbox virtualbox-ext-pack docker.io  `# virtualization and containers`

# Install snap apps
sudo snap install --classic code
sudo snap install discord
sudo snap install brave
sudo snap install --classic microk8s

# Fix PATH variable to include snap packages (should be fine after next reboot)
PATH=$PATH:/snap/bin

# Install vscode extensions
code --install-extension EditorConfig.EditorConfig
code --install-extension dbaeumer.vscode-eslint
code --install-extension ms-python.python
code --install-extension octref.vetur
code --install-extension mhutchie.git-graph
code --install-extension taniarascia.new-moon-vscode

# Teamviewer
#wget -O /tmp/teamviewer_i386.deb https://download.teamviewer.com/download/teamviewer_i386.deb
#sudo gdebi /tmp/teamviewer_i386.deb

# gnome-shell-extension-installer
#wget -O /tmp/gnome-shell-extension-installer "https://github.com/mohelnicek/gnome-shell-extension-installer/raw/master/gnome-shell-extension-installer"
#sudo mv /tmp/gnome-shell-extension-installer /usr/bin/
#sudo chmod +x /usr/bin/gnome-shell-extension-installer

# Gnome shell extensions
gsettings set org.gnome.shell disable-user-extensions true
sleep 10
# Enable
gnome-extensions enable 'caffeine@patapon.info'
gnome-extensions enable 'openweather-extension@jenslody.de'
gnome-extensions enable 'bettervolume@tudmotu.com'
gnome-extensions enable 'dash-to-panel@jderose9.github.com'
gnome-extensions enable 'system-monitor@paradoxxx.zero.gmail.com'
# Disable
gnome-extensions disable 'desktop-icons@csoriano'
gnome-extensions disable 'ubuntu-appindicators@ubuntu.com'
gsettings set org.gnome.shell disable-user-extensions false
#gnome-shell --replace >/dev/null 2>/dev/null &


# Fix sources (idk why it does that :/)
[[ $(grep -c "deb http" /etc/apt/sources.list.d/google-earth-pro.list 2>/dev/null) -eq 1 ]] && sudo sed -i '/deb http/s/deb http/deb [arch=amd64] http/g' /etc/apt/sources.list.d/google-earth-pro.list


# Removing unwanted packages
sudo apt purge -y geary
sudo apt autoremove -y
